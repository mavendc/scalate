package com.mavendc.scalate

import org.osgi.framework.{BundleActivator,BundleContext}
import org.slf4j.{Logger,LoggerFactory}


class Activator extends BundleActivator {

  private val log: Logger = LoggerFactory.getLogger(classOf[Activator])

  override def start(bundleContext: BundleContext) {
    log.info("Scalate Support started")
  }

  override def stop(bundleContext: BundleContext) {
    log.info("Scalate Support stopped")
  }
}
