package com.mavendc.scalate.scripting

import javax.script.{ScriptEngineFactory, Bindings, ScriptContext, AbstractScriptEngine}
import java.io.Reader
import org.apache.felix.scr.annotations.{Component,Service}

/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 12/17/13
 * Time: 5:30 PM
 */
object ScalateScriptEngine {
}

class ScalateScriptEngine(factory: ScalateScriptEngineFactory) extends AbstractScriptEngine {

  def eval(p1: Reader, p2: ScriptContext): AnyRef = null

  def eval(p1: String, p2: ScriptContext): AnyRef = null

  def createBindings(): Bindings = null

  def getFactory: ScriptEngineFactory = factory


}
