package com.mavendc.scalate.scripting

import javax.script.{ScriptEngine, ScriptEngineFactory}
import org.slf4j.LoggerFactory
import java.util.{Collections,List}
import org.apache.felix.scr.annotations.{Component,Service}

/**
 * Created with IntelliJ IDEA.
 * User: John Dorrance
 * Date: 12/17/13
 * Time: 5:28 PM
 */

object ScalateScriptEngineFactory{
  private val log = LoggerFactory.getLogger(classOf[ScalateScriptEngineFactory])
  private val NL = System.getProperty("line.separator")

  val SCALATE_SETTINGS = "scalate.settings"
  val SCALA_REPORTER = "scalate.reporter"
  val SCALA_CLASSPATH_X = "scalate.classpath.x"

  val ENGINE_NAME = "Scalate Scripting Engine"
  val LANGUAGE_VERSION = "2.8.1"
  val ENGINE_VERSION = "0.9/scala " + LANGUAGE_VERSION
  val EXTENSIONS = Collections.singletonList("ssp")
  val LANGUAGE_NAME = "Scalate"
  val MIME_TYPES = Collections.singletonList("application/x-scalate")
  val NAMES = Collections.singletonList("scalate")
}
@Service
@Component
class ScalateScriptEngineFactory extends ScriptEngineFactory{
  import ScalateScriptEngineFactory._

  def getEngineName: String =  ENGINE_NAME
  def getEngineVersion: String = ENGINE_VERSION
  def getExtensions: List[String] = EXTENSIONS
  def getLanguageName: String = LANGUAGE_NAME
  def getLanguageVersion: String = LANGUAGE_VERSION
  def getMimeTypes: List[String] = MIME_TYPES
  def getNames: List[String] = NAMES

  def getParameter(key: String): String = key.toUpperCase match {
    case ScriptEngine.ENGINE => getEngineName
    case ScriptEngine.ENGINE_VERSION => getEngineVersion
    case ScriptEngine.NAME => getNames.get(0)
    case ScriptEngine.LANGUAGE =>  getLanguageName
    case ScriptEngine.LANGUAGE_VERSION => getLanguageVersion
    case "threading" => "multithreaded"
    case _ => null
  }

  def getMethodCallSyntax(obj: String, method: String, args: String*): String =
    obj + "." + method + "(" + args.mkString(",") + ")"

  def getOutputStatement(toDisplay: String): String =
    "println(\""+ toDisplay+ "\")"

  def getProgram(statements: String*): String = {
    ""
  }

  def getScriptEngine: ScriptEngine = new ScalateScriptEngine(this)


}
